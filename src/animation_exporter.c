#include "animation_exporter.h"
#include "file_utilities.h"
#include "entity_utilities.h"

#include <kazmath/mat3.h>

#include <stdlib.h>

extern uint32 numBoneOffsets;
extern BoneOffset *boneOffsets;

typedef struct joint_t {
	char name[1024];
	UUID id;
	kmMat4 transform;
	UUID parent;
	UUID firstChild;
	UUID nextSibling;
} Joint;

internal UUID rootNodeID;
internal uint32 numJoints;
internal Joint *joints;

#define SKELETON_FOLDER "skeleton"

internal UUID entityID;

internal void writeTransform(const kmMat4 *transform, FILE *file);

internal void createSkeleton(const struct aiNode *rootNode);
internal void getNumJoints(const struct aiNode *node);
internal void createJoints(const struct aiNode *node, UUID parent);
internal void addChildToParent(UUID parent, UUID child);
internal void writeCollisionPrimitiveParents(cJSON *assetData);
internal void writeJoints(const char *skeletonFolder, Log log);

internal Joint* getJoint(UUID id);
internal Joint* getJointByName(const char *name);

extern struct aiNode* getNode(struct aiNode* node, const char *name);

int32 exportAnimations(
	const struct aiScene *scene,
	const char *parentFolder,
	cJSON *assetData,
	FILE *file,
	Log log
) {
	char *skeletonFolder = NULL;
	if (parentFolder) {
		skeletonFolder = malloc(
			strlen(parentFolder) + strlen(SKELETON_FOLDER) + 2
		);

		sprintf(skeletonFolder, "%s/%s", parentFolder, SKELETON_FOLDER);
	} else {
		skeletonFolder = malloc(strlen(SKELETON_FOLDER) + 1);
		strcpy(skeletonFolder, SKELETON_FOLDER);
	}

	deleteFolder(skeletonFolder, false, log);

	if (scene->mNumAnimations == 0) {
		free(skeletonFolder);
		return 0;
	}

	fwrite(&numBoneOffsets, sizeof(uint32), 1, file);
	for (uint32 i = 0; i < numBoneOffsets; i++) {
		BoneOffset *boneOffset = &boneOffsets[i];
		writeStringAsUUID(boneOffset->name, file);
		writeTransform(&boneOffset->offset, file);
	}

	struct aiNode *armature = getNode(scene->mRootNode, "Armature");
	if (!armature) {
		armature = scene->mRootNode;
	}

	createSkeleton(armature);
	MKDIR(skeletonFolder);
	writeCollisionPrimitiveParents(assetData);
	writeJoints(skeletonFolder, log);

	free(skeletonFolder);

	cJSON *animation = cJSON_AddObjectToObject(assetData, "animation");

	cJSON_AddStringToObject(animation, "skeleton", rootNodeID.string);
	cJSON_AddStringToObject(animation, "entity", entityID.string);
	cJSON *animationNames = cJSON_AddObjectToObject(animation, "names");

	fwrite(&scene->mNumAnimations, sizeof(uint32), 1, file);
	for (uint32 i = 0; i < scene->mNumAnimations; i++) {
		struct aiAnimation *animation = scene->mAnimations[i];

		const char *name = strrchr(animation->mName.data, '|');
		if (!name) {
			name = animation->mName.data;
		} else {
			name += 1;
		}

		writeStringAsUUID(name, file);

		fwrite(&animation->mTicksPerSecond, sizeof(real64), 1, file);

		real64 keyFrameDuration = 1.0 / animation->mTicksPerSecond;
		real64 duration = 0.0;

		fwrite(&animation->mNumChannels, sizeof(uint32), 1, file);
		for (uint32 j = 0; j < animation->mNumChannels; j++) {
			struct aiNodeAnim *channel = animation->mChannels[j];

			writeStringAsUUID(channel->mNodeName.data, file);

			fwrite(&channel->mNumPositionKeys, sizeof(uint32), 1, file);
			for (uint32 k = 0; k < channel->mNumPositionKeys; k++) {
				struct aiVectorKey *positionKeyFrame =
					&channel->mPositionKeys[k];

				real64 time = keyFrameDuration * positionKeyFrame->mTime;
				fwrite(&time, sizeof(real64), 1, file);

				if (time > duration) {
					duration = time;
				}

				kmVec3 position;
				kmVec3Fill(
					&position,
					positionKeyFrame->mValue.x,
					positionKeyFrame->mValue.y,
					positionKeyFrame->mValue.z
				);

				fwrite(&position, sizeof(kmVec3), 1, file);
			}

			fwrite(&channel->mNumRotationKeys, sizeof(uint32), 1, file);
			for (uint32 k = 0; k < channel->mNumRotationKeys; k++) {
				struct aiQuatKey *rotationKeyFrame =
					&channel->mRotationKeys[k];

				real64 time = keyFrameDuration * rotationKeyFrame->mTime;
				fwrite(&time, sizeof(real64), 1, file);

				if (time > duration) {
					duration = time;
				}

				kmQuaternion rotation;
				kmQuaternionFill(
					&rotation,
					rotationKeyFrame->mValue.x,
					rotationKeyFrame->mValue.y,
					rotationKeyFrame->mValue.z,
					rotationKeyFrame->mValue.w
				);
				kmQuaternionNormalize(&rotation, &rotation);

				fwrite(&rotation, sizeof(kmQuaternion), 1, file);
			}

			fwrite(&channel->mNumScalingKeys, sizeof(uint32), 1, file);
			for (uint32 k = 0; k < channel->mNumScalingKeys; k++) {
				struct aiVectorKey *scaleKeyFrame =
					&channel->mScalingKeys[k];

				real64 time = keyFrameDuration * scaleKeyFrame->mTime;
				fwrite(&time, sizeof(real64), 1, file);

				if (time > duration) {
					duration = time;
				}

				kmVec3 scale;
				kmVec3Fill(
					&scale,
					scaleKeyFrame->mValue.x,
					scaleKeyFrame->mValue.y,
					scaleKeyFrame->mValue.z
				);

				fwrite(&scale, sizeof(kmVec3), 1, file);
			}
		}

		fwrite(&duration, sizeof(real64), 1, file);
		cJSON_AddNumberToObject(animationNames, name, duration);
	}

	free(joints);
	memset(rootNodeID.string, 0, sizeof(UUID));

	return 0;
}

kmMat4 convertToKazmathMatrix(const struct aiMatrix4x4 *aiMatrix) {
	kmMat4 matrix;
	real32 *matrixData = matrix.mat;

	matrixData[0] = aiMatrix->a1;
	matrixData[1] = aiMatrix->b1;
	matrixData[2] = aiMatrix->c1;
	matrixData[3] = aiMatrix->d1;
	matrixData[4] = aiMatrix->a2;
	matrixData[5] = aiMatrix->b2;
	matrixData[6] = aiMatrix->c2;
	matrixData[7] = aiMatrix->d2;
	matrixData[8] = aiMatrix->a3;
	matrixData[9] = aiMatrix->b3;
	matrixData[10] = aiMatrix->c3;
	matrixData[11] = aiMatrix->d3;
	matrixData[12] = aiMatrix->a4;
	matrixData[13] = aiMatrix->b4;
	matrixData[14] = aiMatrix->c4;
	matrixData[15] = aiMatrix->d4;

	return matrix;
}

void decomposeTransform(
	const kmMat4 *transform,
	kmVec3 *position,
	kmQuaternion *rotation,
	kmVec3 *scale,
	bool removeRotationScale
) {
	if (position) {
		kmMat4ExtractTranslationVec3(transform, position);
	}

	kmVec3 xAxis;
	kmVec3Fill(
		&xAxis,
		transform->mat[0],
		transform->mat[1],
		transform->mat[2]
	);

	kmVec3 yAxis;
	kmVec3Fill(
		&yAxis,
		transform->mat[4],
		transform->mat[5],
		transform->mat[6]
	);

	kmVec3 zAxis;
	kmVec3Fill(
		&zAxis,
		transform->mat[8],
		transform->mat[9],
		transform->mat[10]
	);

	kmVec3 scaleValues;
	kmVec3Fill(
		&scaleValues,
		kmVec3Length(&xAxis),
		kmVec3Length(&yAxis),
		kmVec3Length(&zAxis)
	);

	kmMat4 nonScaledTransform;
	kmMat4Assign(&nonScaledTransform, transform);
	nonScaledTransform.mat[0] /= scaleValues.x;
	nonScaledTransform.mat[1] /= scaleValues.x;
	nonScaledTransform.mat[2] /= scaleValues.x;
	nonScaledTransform.mat[4] /= scaleValues.y;
	nonScaledTransform.mat[5] /= scaleValues.y;
	nonScaledTransform.mat[6] /= scaleValues.y;
	nonScaledTransform.mat[8] /= scaleValues.z;
	nonScaledTransform.mat[9] /= scaleValues.z;
	nonScaledTransform.mat[10] /= scaleValues.z;

	if (rotation) {
		kmMat3 rotationMatrix;

		if (removeRotationScale) {
			kmMat4ExtractRotationMat3(&nonScaledTransform, &rotationMatrix);
		} else {
			kmMat4ExtractRotationMat3(transform, &rotationMatrix);
		}

		kmQuaternionRotationMatrix(rotation, &rotationMatrix);
		kmQuaternionNormalize(rotation, rotation);
	}

	if (scale) {
		kmVec3Assign(scale, &scaleValues);
	}
}

void writeTransform(const kmMat4 *transform, FILE *file) {
	kmVec3 position;
	kmQuaternion rotation;
	kmVec3 scale;
	decomposeTransform(transform, &position, &rotation, &scale, false);

	fwrite(&position, sizeof(kmVec3), 1, file);
	fwrite(&rotation, sizeof(kmQuaternion), 1, file);
	fwrite(&scale, sizeof(kmVec3), 1, file);
}

void createSkeleton(const struct aiNode *node) {
	numJoints = 0;
	getNumJoints(node);

	joints = calloc(numJoints, sizeof(Joint));

	UUID parent;
	memset(&parent, 0, sizeof(UUID));

	numJoints = 0;
	createJoints(node, parent);
}

void getNumJoints(const struct aiNode *node) {
	numJoints++;
	for (uint32 i = 0; i < node->mNumChildren; i++) {
		getNumJoints(node->mChildren[i]);
	}
}

void createJoints(const struct aiNode *node, UUID parent) {
	char *prefix = getPrefix(node->mName.data, "_");
	if (prefix && !strcmp(prefix, "col")) {
		free(prefix);
		return;
	}

	free(prefix);

	Joint *joint = &joints[numJoints++];

	strcpy(joint->name, node->mName.data);
	joint->id = generateUUID();
	if (strlen(rootNodeID.string) == 0) {
		rootNodeID = joint->id;
	}

	joint->transform = convertToKazmathMatrix(&node->mTransformation);
	joint->parent = parent;

	addChildToParent(joint->parent, joint->id);

	for (uint32 i = 0; i < node->mNumChildren; i++) {
		createJoints(node->mChildren[i], joint->id);
	}
}

void addChildToParent(UUID parent, UUID child) {
	Joint *parentJoint = getJoint(parent);
	if (parentJoint) {
		Joint *childJoint = getJoint(parentJoint->firstChild);

		if (!childJoint) {
			parentJoint->firstChild = child;
		} else {
			Joint *siblingJoint = NULL;
			Joint *nextSiblingJoint = childJoint;

			do {
				siblingJoint = nextSiblingJoint;
				nextSiblingJoint = getJoint(nextSiblingJoint->nextSibling);
			} while (nextSiblingJoint);

			siblingJoint->nextSibling = child;
		}
	}
}

void writeJoints(const char *skeletonFolder, Log log) {
	for (uint32 i = 0; i < numJoints; i++) {
		cJSON *json = cJSON_CreateObject();

		Joint *joint = &joints[i];

		cJSON_AddStringToObject(json, "uuid", joint->id.string);

		cJSON *components = cJSON_AddObjectToObject(json, "components");

		kmVec3 position;
		kmQuaternion rotation;
		kmVec3 scale;
		decomposeTransform(
			&joint->transform,
			&position,
			&rotation,
			&scale,
			false
		);

		if (strlen(joint->parent.string) == 0) {
			entityID = generateUUID();
			joint->parent = entityID;
		}

		addTransform(
			components,
			(real32*)&position,
			(real32*)&rotation,
			(real32*)&scale,
			&joint->parent,
			&joint->firstChild,
			&joint->nextSibling
		);

		cJSON *jsonJoint =  cJSON_AddArrayToObject(components, "joint");

		addString(jsonJoint, "name", 64, joint->name);

		char *filename = malloc(
			strlen(skeletonFolder) + strlen(joint->name) + 2
		);

		sprintf(filename, "%s/%s", skeletonFolder, joint->name);

		writeJSON(json, filename, true, log);
		free(filename);

		cJSON_Delete(json);
	}
}

void writeCollisionPrimitiveParents(cJSON *assetData) {
	cJSON *physics = cJSON_GetObjectItem(assetData, "physics");
	cJSON *collisionPrimitives = cJSON_GetObjectItem(
		physics,
		"collision_primitives"
	);

	cJSON *boxes = cJSON_GetObjectItem(collisionPrimitives, "boxes");

	cJSON *box = boxes->child;
	while (box) {
		cJSON *parent = cJSON_GetObjectItem(box, "parent");

		if (parent) {
			Joint *joint = getJointByName(parent->valuestring);
			if (joint) {
				cJSON *uuid = cJSON_CreateString(joint->id.string);
				cJSON_ReplaceItemInObject(box, "parent", uuid);

				cJSON *entity = cJSON_GetObjectItem(box, "entity");
				addChildToParent(joint->id, stringToUUID(entity->valuestring));
			} else {
				cJSON_DeleteItemFromObject(box, "parent");
			}
		}

		box = box->next;
	}

	cJSON *spheres = cJSON_GetObjectItem(collisionPrimitives, "spheres");

	cJSON *sphere = spheres->child;
	while (sphere) {
		cJSON *parent = cJSON_GetObjectItem(sphere, "parent");

		if (parent) {
			Joint *joint = getJointByName(parent->valuestring);
			if (joint) {
				cJSON *uuid = cJSON_CreateString(joint->id.string);
				cJSON_ReplaceItemInObject(sphere, "parent", uuid);

				cJSON *entity = cJSON_GetObjectItem(sphere, "entity");
				addChildToParent(joint->id, stringToUUID(entity->valuestring));
			} else {
				cJSON_DeleteItemFromObject(sphere, "parent");
			}
		}

		sphere = sphere->next;
	}

	cJSON *capsules = cJSON_GetObjectItem(collisionPrimitives, "capsules");

	cJSON *capsule = capsules->child;
	while (capsule) {
		cJSON *parent = cJSON_GetObjectItem(capsule, "parent");

		if (parent) {
			Joint *joint = getJointByName(parent->valuestring);
			if (joint) {
				cJSON *uuid = cJSON_CreateString(joint->id.string);
				cJSON_ReplaceItemInObject(capsule, "parent", uuid);

				cJSON *entity = cJSON_GetObjectItem(capsule, "entity");
				addChildToParent(joint->id, stringToUUID(entity->valuestring));
			} else {
				cJSON_DeleteItemFromObject(capsule, "parent");
			}
		}

		capsule = capsule->next;
	}
}

Joint* getJoint(UUID id) {
	if (strlen(id.string) > 0) {
		for (uint32 i = 0; i < numJoints; i++) {
			if (!strcmp(id.string, joints[i].id.string)) {
				return &joints[i];
			}
		}
	}

	return NULL;
}

Joint* getJointByName(const char *name) {
	if (name && strlen(name) > 0) {
		for (uint32 i = 0; i < numJoints; i++) {
			if (!strcmp(name, joints[i].name)) {
				return &joints[i];
			}
		}
	}

	return NULL;
}