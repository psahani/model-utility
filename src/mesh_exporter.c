#include "mesh_exporter.h"
#include "animation_exporter.h"
#include "file_utilities.h"
#include "log.h"

#include <assimp/cimport.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <kazmath/vec2.h>
#include <kazmath/vec3.h>
#include <kazmath/vec4.h>
#include <kazmath/mat4.h>

#include <stdio.h>
#include <malloc.h>

#define DELIMITER "__"

#define NUM_BONES 4
#define MAX_BONE_COUNT 128

internal const uint8 binaryFileVersion = 1;

uint32 numBoneOffsets;
BoneOffset *boneOffsets;

typedef struct vertex_t {
	kmVec3 position;
	kmVec4 color;
	kmVec3 normal;
	kmVec3 tangent;
	kmVec3 bitangent;
	kmVec2 materialUV;
	kmVec2 maskUV;
	uint32 bones[NUM_BONES];
	real32 weights[NUM_BONES];
} Vertex;

internal void writeSubsets(
	const struct aiNode *node,
	const struct aiScene *scene,
	cJSON *assetData,
	FILE *file
);

internal void writeSubset(
	const struct aiMesh *subset,
	const char *nodeName,
	cJSON *assetData,
	FILE *file
);

internal cJSON* getMesh(const char *name, cJSON *assetData);
internal bool getMaterialUVInformation(cJSON *mesh, uint32 *uvMap);
internal bool getMaskUVInformation(cJSON *mesh, uint32 *uvMap);

internal int32 getBoneIndex(struct aiBone *bone);

int32 exportMesh(const char *filename, Log log) {
	int32 error = 0;

	char *modelFilename = getFullFilePath(filename, MODEL_FILE_EXTENSION, NULL);

	struct aiPropertyStore *properties = aiCreatePropertyStore();
	aiSetImportPropertyInteger(
		properties,
		AI_CONFIG_PP_RVC_FLAGS,
		aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS
	);

	const struct aiScene *scene = aiImportFile(
		modelFilename,
		(aiProcessPreset_TargetRealtime_Quality & ~aiProcess_SplitLargeMeshes) |
		aiProcess_RemoveComponent |
		aiProcess_OptimizeMeshes
	);

	if (!scene) {
		LOG(log, "%s\n", aiGetErrorString());
		error = -1;
	}

	free(modelFilename);

	if (error != -1) {
		char *meshFilename = getFullFilePath(filename, "mesh", NULL);
		FILE *file = fopen(meshFilename, "wb");
		free(meshFilename);

		fwrite(&binaryFileVersion, sizeof(uint8), 1, file);

		numBoneOffsets = 0;
		boneOffsets = NULL;

		cJSON *assetData = loadJSON(filename, log);
		writeSubsets(scene->mRootNode, scene, assetData, file);

		bool hasAnimations = scene->mNumAnimations > 0;
		fwrite(&hasAnimations, sizeof(bool), 1, file);

		char *parentFolder = getParentFolder(filename);
		error = exportAnimations(scene, parentFolder, assetData, file, log);
		free(parentFolder);

		writeJSON(assetData, filename, true, log);
		cJSON_Delete(assetData);

		free(boneOffsets);

		fclose(file);
	}

	aiReleaseImport(scene);
	aiReleasePropertyStore(properties);

	return error;
}

void writeSubsets(
	const struct aiNode *node,
	const struct aiScene *scene,
	cJSON *assetData,
	FILE *file
) {
	if (node->mNumMeshes > 0) {
		if (node->mNumMeshes > 1) {
			for (uint32 i = 0; i < node->mNumMeshes; i++) {
				char name[2048];
				sprintf(name, "%s_%d", node->mName.data, i);

				writeSubset(
					scene->mMeshes[node->mMeshes[i]],
					name,
					assetData,
					file
				);
			}
		} else {
			writeSubset(
				scene->mMeshes[node->mMeshes[0]],
				node->mName.data,
				assetData,
				file
			);
		}
	}

	for (uint32 i = 0; i < node->mNumChildren; i++) {
		writeSubsets(node->mChildren[i], scene, assetData, file);
	}
}

void writeSubset(
	const struct aiMesh *subset,
	const char *nodeName,
	cJSON *assetData,
	FILE *file
) {
	char *prefix = getPrefix(nodeName, "_");
	if (prefix && !strcmp(prefix, "col")) {
		free(prefix);
		return;
	}

	free(prefix);

	const struct aiVector3D *positions = subset->mVertices;
	const struct aiColor4D *colors = subset->mColors[0];
	const struct aiVector3D *normals = subset->mNormals;
	const struct aiVector3D *tangents = subset->mTangents;
	const struct aiVector3D *bitangents = subset->mBitangents;

	cJSON *mesh = getMesh(nodeName, assetData);

	uint32 materialUVMap;
	bool swizzleMaterialUV = getMaterialUVInformation(mesh, &materialUVMap);

	uint32 maskUVMap;
	bool swizzleMaskUV = getMaskUVInformation(mesh, &maskUVMap);

	const struct aiVector3D *materialUVs = subset->mTextureCoords[
		materialUVMap
	];

	if (!materialUVs) {
		materialUVs = subset->mTextureCoords[0];
	}

	const struct aiVector3D *maskUVs = subset->mTextureCoords[maskUVMap];
	if (!maskUVs) {
		maskUVs = subset->mTextureCoords[0];
	}

	uint32 numVertices = 0;
	Vertex *vertices = calloc(subset->mNumVertices, sizeof(Vertex));
	uint32 numIndices = 0;
	uint32 *indices = calloc(subset->mNumFaces * 3, sizeof(uint32));

	uint32 i, j;
	for (i = 0; i < subset->mNumVertices; i++, numVertices++) {
		Vertex *vertex = &vertices[numVertices];

		memcpy(&vertex->position, &positions[i], sizeof(kmVec3));

		if (colors) {
			memcpy(&vertex->color, &colors[i], sizeof(kmVec4));
		} else {
			memset(&vertex->color, 0, sizeof(kmVec3));
			vertex->color.w = 1.0f;
		}

		memcpy(&vertex->normal, &normals[i], sizeof(kmVec3));

		if (tangents) {
			memcpy(&vertex->tangent, &tangents[i], sizeof(kmVec3));
		}

		if (bitangents) {
			memcpy(&vertex->bitangent, &bitangents[i], sizeof(kmVec3));
		}

		if (materialUVs) {
			vertex->materialUV.x = materialUVs[i].x;
			vertex->materialUV.y = swizzleMaterialUV ?
				1.0f - materialUVs[i].y : materialUVs[i].y;
		}

		if (maskUVs) {
			vertex->maskUV.x = maskUVs[i].x;
			vertex->maskUV.y = swizzleMaskUV ?
				1.0f - maskUVs[i].y : maskUVs[i].y;
		}
	}

	for (i = 0; i < subset->mNumFaces; ++i) {
		for (j = 0; j < 3; ++j) {
			indices[numIndices++] = subset->mFaces[i].mIndices[j];
		}
	}

	uint32 subsetNumBones = 0;
	for (i = 0; i < subset->mNumBones; i++) {
		struct aiBone *bone = subset->mBones[i];
		if (getBoneIndex(bone) == -1) {
			subsetNumBones++;
		}
	}

	uint32 newNumBones = numBoneOffsets + subsetNumBones;
	if (newNumBones >= MAX_BONE_COUNT) {
		newNumBones = MAX_BONE_COUNT;
	}

	if (numBoneOffsets == 0) {
		boneOffsets = malloc(newNumBones * sizeof(BoneOffset));
	} else {
		boneOffsets = realloc(boneOffsets, newNumBones * sizeof(BoneOffset));
	}

	for (i = 0; i < subset->mNumBones; i++) {
		if (numBoneOffsets == MAX_BONE_COUNT) {
			break;
		}

		struct aiBone *bone = subset->mBones[i];
		uint32 boneIndex = getBoneIndex(bone);

		if (boneIndex == -1) {
			boneIndex = numBoneOffsets;
			BoneOffset *boneOffset = &boneOffsets[boneIndex];

			strcpy(boneOffset->name, bone->mName.data);
			boneOffset->offset = convertToKazmathMatrix(&bone->mOffsetMatrix);

			numBoneOffsets++;
		}

		for (j = 0; j < bone->mNumWeights; j++) {
			struct aiVertexWeight *vertexWeight = &bone->mWeights[j];

			Vertex *vertex = &vertices[vertexWeight->mVertexId];

			int32 weightIndex = -1;
			for (uint8 k = 0; k < NUM_BONES; k++) {
				if (vertex->weights[k] == 0.0f) {
					weightIndex = k;
					break;
				}
			}

			if (weightIndex == -1) {
				for (uint8 k = 0; k < NUM_BONES; k++) {
					if (vertexWeight->mWeight > vertex->weights[k]) {
						weightIndex = k;
						break;
					}
				}
			}

			if (weightIndex > -1) {
				vertex->weights[weightIndex] = vertexWeight->mWeight;
				vertex->bones[weightIndex] = boneIndex;
			}
		}
	}

	for (i = 0; i < numVertices; i++) {
		Vertex *vertex = &vertices[i];

		real32 weightSum = 0.0f;
		for (uint8 k = 0; k < NUM_BONES; k++) {
			weightSum += vertex->weights[k];
		}

		if (weightSum > 0.0f) {
			for (uint8 k = 0; k < NUM_BONES; k++) {
				vertex->weights[k] /= weightSum;
			}
		}
	}

	fwrite(&numVertices, sizeof(uint32), 1, file);
	fwrite(vertices, sizeof(Vertex), numVertices, file);
	fwrite(&numIndices, sizeof(uint32), 1, file);
	fwrite(indices, sizeof(uint32), numIndices, file);

	free(vertices);
	free(indices);
}

cJSON* getMesh(const char *name, cJSON *assetData) {
	char *meshName = getSuffix(name, DELIMITER);
	if (!meshName) {
		meshName = malloc(strlen(name) + 1);
		strcpy(meshName, name);
	}

	cJSON *mesh = cJSON_GetObjectItem(assetData, "meshes")->child;
	while (mesh) {
		if (!strcmp(mesh->string, meshName)) {
			free(meshName);
			return mesh;
		}

		mesh = mesh->next;
	}

	free(meshName);

	return NULL;
}

bool getMaterialUVInformation(cJSON *mesh, uint32 *uvMap) {
	cJSON *material = cJSON_GetObjectItem(mesh, "material");
	cJSON *uv = cJSON_GetObjectItem(material, "uv");

	*uvMap = cJSON_GetObjectItem(uv, "map")->valueint;
	cJSON *swizzle = cJSON_GetObjectItem(uv, "swizzle");

	return cJSON_IsTrue(swizzle) ? true : false;
}

bool getMaskUVInformation(cJSON *mesh, uint32 *uvMap) {
	cJSON *masks = cJSON_GetObjectItem(mesh, "masks");
	cJSON *uv = cJSON_GetObjectItem(masks, "uv");

	*uvMap = cJSON_GetObjectItem(uv, "map")->valueint;
	cJSON *swizzle = cJSON_GetObjectItem(uv, "swizzle");

	return cJSON_IsTrue(swizzle) ? true : false;
}

int32 getBoneIndex(struct aiBone *bone) {
	for (uint32 i = 0; i < numBoneOffsets; i++) {
		if (!strcmp(boneOffsets[i].name, bone->mName.data)) {
			return i;
		}
	}

	return -1;
}