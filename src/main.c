#include "definitions.h"
#include "file_utilities.h"
#include "mesh_exporter.h"
#include "material_exporter.h"

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>

internal void showHelp(char *extension);
internal int32 showError(char *extension);

int32 main(int32 argc, char *argv[]) {
	int32 error = 0;

	srand(time(0));

	char *extension = "";

	#if defined _WIN32
	extension = ".exe";
	#endif

	switch (argc - 1) {
		case 1:
			if (!strcmp(argv[1], "--help")) {
				showHelp(extension);
			} else {
				char *extension = getExtension(argv[1]);
				if (extension && !strcmp(extension, MODEL_FILE_EXTENSION)) {
					char *filename = removeExtension(argv[1]);
					error = exportMaterials(filename, false, (Log)&printf);
					if (error != -1) {
						error = exportMesh(filename, (Log)&printf);
					}

					free(filename);
					free(extension);
				} else {
					error = exportMaterials(argv[1], false, (Log)&printf);
					if (error != -1) {
						error = exportMesh(argv[1], (Log)&printf);
					}
				}
			}
			break;
		default:
			error = showError(extension);
			break;
	}

	return error;
}

void showHelp(char *extension) {
	printf("Usage:\n");
	printf("model-utility%s [FILENAME]\n", extension);
}

int32 showError(char *extension) {
	printf("Invalid arguments.\n");
	printf("Try 'model-utility%s --help' for more information.\n", extension);
	return -1;
}