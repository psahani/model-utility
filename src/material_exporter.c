#include "material_exporter.h"
#include "animation_exporter.h"
#include "file_utilities.h"
#include "log.h"

#include <assimp/cimport.h>
#include <assimp/material.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <kazmath/mat4.h>

#include <malloc.h>

#define DELIMITER "__"

#define NUM_MATERIAL_COMPONENTS 7

internal const char *materialValueNames[NUM_MATERIAL_COMPONENTS] = {
	"ambient_occlusion",
	"base_color",
	"emissive",
	"height",
	"metallic",
	"normal",
	"roughness"
};

internal void writeSubsets(
	const struct aiNode *node,
	const struct aiScene *scene,
	cJSON *meshes
);

internal void writeSubset(
	const struct aiMesh *subset,
	const char *nodeName,
	const struct aiScene *scene,
	cJSON *meshes
);

internal void writeMaterial(
	cJSON *material,
	const char *name,
	int32 uvMap
);

internal void writePhysics(const struct aiScene *scene, cJSON *physics);
internal void getCollisionPrimitives(
	const struct aiScene *scene,
	struct aiNode ***boxes,
	uint32 *numBoxes,
	struct aiNode ***spheres,
	uint32 *numSpheres,
	struct aiNode ***capsules,
	uint32 *numCapsules
);

struct aiNode* getNode(struct aiNode* node, const char *name);
internal void getNodesByPrefix(
	struct aiNode *node,
	struct aiNode ***nodes,
	uint32 *numNodes,
	const char *prefix,
	const char *delimiter
);

internal const char *modelName;

int32 exportMaterials(
	const char *filename,
	bool collisionPrimitivesOnly,
	Log log
) {
	const char *backslash = strrchr(filename, '/');
	if (backslash) {
		modelName = backslash + 1;
	} else {
		modelName = filename;
	}

	int32 error = 0;

	char *modelFilename = getFullFilePath(filename, MODEL_FILE_EXTENSION, NULL);

	struct aiPropertyStore *properties = aiCreatePropertyStore();
	aiSetImportPropertyInteger(
		properties,
		AI_CONFIG_PP_RVC_FLAGS,
		aiComponent_TEXTURES | aiComponent_LIGHTS | aiComponent_CAMERAS
	);

	const struct aiScene *scene = aiImportFile(
		modelFilename,
		(aiProcessPreset_TargetRealtime_Quality & ~aiProcess_SplitLargeMeshes) |
		aiProcess_RemoveComponent |
		aiProcess_OptimizeMeshes
	);

	if (!scene) {
		LOG(log, "%s\n", aiGetErrorString());
		error = -1;
	}

	free(modelFilename);

	if (error != -1) {
		cJSON* json = NULL;
		if (collisionPrimitivesOnly) {
			json = loadJSON(filename, log);
			cJSON_DeleteItemFromObject(json, "physics");
		} else {
			json = cJSON_CreateObject();

			cJSON *meshes = cJSON_AddObjectToObject(json, "meshes");
			writeSubsets(scene->mRootNode, scene, meshes);
		}

		cJSON *physics = cJSON_AddObjectToObject(json, "physics");
		writePhysics(scene, physics);

		writeJSON(json, filename, true, log);
		cJSON_Delete(json);
	}

	aiReleaseImport(scene);
	aiReleasePropertyStore(properties);

	return error;
}

void writeSubsets(
	const struct aiNode *node,
	const struct aiScene *scene,
	cJSON *meshes
) {
	if (node->mNumMeshes > 0) {
		if (node->mNumMeshes > 1) {
			for (uint32 i = 0; i < node->mNumMeshes; i++) {
				char name[2048];
				sprintf(name, "%s_%d", node->mName.data, i);

				writeSubset(
					scene->mMeshes[node->mMeshes[i]],
					name,
					scene,
					meshes
				);
			}
		} else {
			writeSubset(
				scene->mMeshes[node->mMeshes[0]],
				node->mName.data,
				scene,
				meshes
			);
		}
	}

	for (uint32 i = 0; i < node->mNumChildren; i++) {
		writeSubsets(node->mChildren[i], scene, meshes);
	}
}

void writeSubset(
	const struct aiMesh *subset,
	const char *nodeName,
	const struct aiScene *scene,
	cJSON *meshes
) {
	char *prefix = getPrefix(nodeName, "_");
	if (prefix && !strcmp(prefix, "col")) {
		free(prefix);
		return;
	}

	free(prefix);

	char *meshName = getSuffix(nodeName, DELIMITER);
	if (!meshName) {
		meshName = malloc(strlen(nodeName) + 1);
		strcpy(meshName, nodeName);
	}

	cJSON *mesh = cJSON_AddObjectToObject(meshes, meshName);
	free(meshName);

	cJSON *jsonMaterial = cJSON_AddObjectToObject(mesh, "material");

	const struct aiMaterial *material =
		scene->mMaterials[subset->mMaterialIndex];

	int32 uvMap;
	enum aiReturn aiError = aiGetMaterialIntegerArray(
		material,
		AI_MATKEY_UVWSRC(aiTextureType_DIFFUSE, 0),
		&uvMap,
		NULL
	);

	if (aiError != AI_SUCCESS) {
		uvMap = 0;
	}

	char *materialName = getPrefix(nodeName, DELIMITER);
	if (!materialName) {
		materialName = malloc(strlen(modelName) + 1);
		strcpy(materialName, modelName);
	}

	writeMaterial(jsonMaterial, materialName, uvMap);
	free(materialName);

	cJSON *masks = cJSON_AddObjectToObject(mesh, "masks");

	cJSON *uv = cJSON_AddObjectToObject(masks, "uv");
	cJSON_AddNumberToObject(uv, "map", uvMap);
	cJSON_AddTrueToObject(uv, "swizzle");

	cJSON *materialMask = cJSON_AddObjectToObject(masks, "material");

	cJSON *collection = cJSON_AddObjectToObject(materialMask, "collection");
	cJSON *grunge = cJSON_AddObjectToObject(materialMask, "grunge");
	cJSON *wear = cJSON_AddObjectToObject(materialMask, "wear");

	writeMaterial(collection, "", -1);
	writeMaterial(grunge, "", -1);
	writeMaterial(wear, "", -1);

	cJSON *opacityMask = cJSON_AddObjectToObject(masks, "opacity");
	cJSON_AddNumberToObject(opacityMask, "value", 1.0);
}

internal void writeMaterial(
	cJSON *material,
	const char *name,
	int32 uvMap
) {
	cJSON_AddStringToObject(material, "name", name);
	cJSON_AddFalseToObject(material, "double-sided");

	if (uvMap > -1) {
		cJSON *uv = cJSON_AddObjectToObject(material, "uv");
		cJSON_AddNumberToObject(uv, "map", uvMap);
		cJSON_AddTrueToObject(uv, "swizzle");
	}

	cJSON *materialValues = cJSON_AddObjectToObject(material, "values");

	real32 value[3] = { 1.0f, 1.0f, 1.0f };

	for (uint8 i = 0; i < NUM_MATERIAL_COMPONENTS; i++) {
		cJSON_AddItemToObject(
			materialValues,
			materialValueNames[i],
			cJSON_CreateFloatArray(value, 3)
		);
	}
}

void writePhysics(const struct aiScene *scene, cJSON *physics) {
	kmMat4 centerOfMassTransform;
	kmMat4Identity(&centerOfMassTransform);

	struct aiNode *centerOfMassNode = getNode(scene->mRootNode, "com");
	if (centerOfMassNode) {
		centerOfMassTransform = convertToKazmathMatrix(
			&centerOfMassNode->mTransformation
		);
	}

	kmVec3 centerOfMassPosition;
	decomposeTransform(
		&centerOfMassTransform,
		&centerOfMassPosition,
		NULL,
		NULL,
		false
	);

	real32 centerOfMass[3];
	memcpy(centerOfMass, &centerOfMassPosition, sizeof(kmVec3));

	cJSON_AddItemToObject(
		physics,
		"center_of_mass",
		cJSON_CreateFloatArray(centerOfMass, 3)
	);

	cJSON *collisionPrimitives = cJSON_AddObjectToObject(
		physics,
		"collision_primitives"
	);

	struct aiNode **boxes = NULL;
	uint32 numBoxes = 0;
	struct aiNode **spheres = NULL;
	uint32 numSpheres = 0;
	struct aiNode **capsules = NULL;
	uint32 numCapsules = 0;

	getCollisionPrimitives(
		scene,
		&boxes,
		&numBoxes,
		&spheres,
		&numSpheres,
		&capsules,
		&numCapsules
	);

	real32 positionValues[3];
	real32 rotationValues[4];
	real32 scaleValues[3];

	kmVec3 position;
	kmQuaternion rotation;
	kmVec3 scale;

	cJSON *jsonBoxes = cJSON_AddObjectToObject(collisionPrimitives, "boxes");
	for (uint32 i = 0; i < numBoxes; i++) {
		kmMat4 transformMatrix = convertToKazmathMatrix(
			&boxes[i]->mTransformation
		);

		decomposeTransform(
			&transformMatrix,
			&position,
			&rotation,
			&scale,
			true
		);

		memcpy(positionValues, &position, sizeof(kmVec3));
		memcpy(rotationValues, &rotation, sizeof(kmQuaternion));
		memcpy(scaleValues, &scale, sizeof(kmVec3));

		char *collisionPrimitiveName = getSuffix(boxes[i]->mName.data, "__");
		cJSON *transform = cJSON_AddObjectToObject(
			jsonBoxes,
			collisionPrimitiveName
		);

		free(collisionPrimitiveName);

		struct aiNode *parent = boxes[i]->mParent;
		if (parent && strcmp(parent->mName.data, "RootNode")) {
			cJSON_AddStringToObject(transform, "parent", parent->mName.data);
		}

		cJSON_AddStringToObject(transform, "entity", generateUUID().string);

		cJSON_AddItemToObject(
			transform,
			"position",
			cJSON_CreateFloatArray(positionValues, 3)
		);

		cJSON_AddItemToObject(
			transform,
			"rotation",
			cJSON_CreateFloatArray(rotationValues, 4)
		);

		cJSON_AddItemToObject(
			transform,
			"scale",
			cJSON_CreateFloatArray(scaleValues, 3)
		);
	}

	free(boxes);

	cJSON *jsonSpheres = cJSON_AddObjectToObject(
		collisionPrimitives,
		"spheres"
	);

	for (uint32 i = 0; i < numSpheres; i++) {
		kmMat4 transformMatrix = convertToKazmathMatrix(
			&spheres[i]->mTransformation
		);

		decomposeTransform(
			&transformMatrix,
			&position,
			&rotation,
			&scale,
			true
		);

		memcpy(positionValues, &position, sizeof(kmVec3));

		char *collisionPrimitiveName = getSuffix(spheres[i]->mName.data, "__");
		cJSON *transform = cJSON_AddObjectToObject(
			jsonSpheres,
			collisionPrimitiveName
		);

		free(collisionPrimitiveName);

		struct aiNode *parent = spheres[i]->mParent;
		if (parent && strcmp(parent->mName.data, "RootNode")) {
			cJSON_AddStringToObject(transform, "parent", parent->mName.data);
		}

		cJSON_AddStringToObject(transform, "entity", generateUUID().string);

		cJSON_AddItemToObject(
			transform,
			"position",
			cJSON_CreateFloatArray(positionValues, 3)
		);

		cJSON_AddNumberToObject(
			transform,
			"scale",
			(scale.x + scale.y + scale.z) / 3
		);
	}

	free(spheres);

	cJSON *jsonCapsules = cJSON_AddObjectToObject(
		collisionPrimitives,
		"capsules"
	);

	for (uint32 i = 0; i < numCapsules; i++) {
		kmMat4 transformMatrix = convertToKazmathMatrix(
			&capsules[i]->mTransformation
		);

		decomposeTransform(
			&transformMatrix,
			&position,
			&rotation,
			&scale,
			true
		);

		memcpy(positionValues, &position, sizeof(kmVec3));
		memcpy(rotationValues, &rotation, sizeof(kmQuaternion));
		memcpy(scaleValues, &scale, sizeof(kmVec3));

		char *collisionPrimitiveName = getSuffix(capsules[i]->mName.data, "__");
		cJSON *transform = cJSON_AddObjectToObject(
			jsonCapsules,
			collisionPrimitiveName
		);

		free(collisionPrimitiveName);

		struct aiNode *parent = capsules[i]->mParent;
		if (parent && strcmp(parent->mName.data, "RootNode")) {
			cJSON_AddStringToObject(transform, "parent", parent->mName.data);
		}

		cJSON_AddStringToObject(transform, "entity", generateUUID().string);

		cJSON_AddItemToObject(
			transform,
			"position",
			cJSON_CreateFloatArray(positionValues, 3)
		);

		cJSON_AddItemToObject(
			transform,
			"rotation",
			cJSON_CreateFloatArray(rotationValues, 4)
		);

		cJSON_AddItemToObject(
			transform,
			"scale",
			cJSON_CreateFloatArray(scaleValues, 3)
		);
	}

	free(capsules);
}

void getCollisionPrimitives(
	const struct aiScene *scene,
	struct aiNode ***boxes,
	uint32 *numBoxes,
	struct aiNode ***spheres,
	uint32 *numSpheres,
	struct aiNode ***capsules,
	uint32 *numCapsules
) {
	struct aiNode **collisionPrimitives = NULL;
	uint32 numCollisionPrimitives = 0;

	getNodesByPrefix(
		scene->mRootNode,
		&collisionPrimitives,
		&numCollisionPrimitives,
		"col",
		"_"
	);

	for (uint32 i = 0; i < numCollisionPrimitives; i++) {
		struct aiNode *collisionPrimitive = collisionPrimitives[i];

		char *prefix = getPrefix(collisionPrimitive->mName.data, DELIMITER);
		if (!prefix) {
			continue;
		}

		char *collisionPrimitiveName = getSuffix(prefix, "_");
		free(prefix);

		if (!collisionPrimitiveName) {
			continue;
		}

		if (!strcmp(collisionPrimitiveName, "box")) {
			if (*numBoxes == 0) {
				*boxes = malloc(++(*numBoxes) * sizeof(struct aiNode*));
			} else {
				*boxes = realloc(
					*boxes,
					++(*numBoxes) * sizeof(struct aiNode*)
				);
			}

			(*boxes)[*numBoxes - 1] = collisionPrimitive;
		} else if (!strcmp(collisionPrimitiveName, "sphere")) {
			if (*numSpheres == 0) {
				*spheres = malloc(++(*numSpheres) * sizeof(struct aiNode*));
			} else {
				*spheres = realloc(
					*spheres,
					++(*numSpheres) * sizeof(struct aiNode*)
				);
			}

			(*spheres)[*numSpheres - 1] = collisionPrimitive;
		} else if (!strcmp(collisionPrimitiveName, "capsule")) {
			if (*numCapsules == 0) {
				*capsules = malloc(++(*numCapsules) * sizeof(struct aiNode*));
			} else {
				*capsules = realloc(
					*capsules,
					++(*numCapsules) * sizeof(struct aiNode*)
				);
			}

			(*capsules)[*numCapsules - 1] = collisionPrimitive;
		}

		free(collisionPrimitiveName);
	}

	free(collisionPrimitives);
}

struct aiNode* getNode(struct aiNode* node, const char *name) {
	if (!strcmp(node->mName.data, name)) {
		return node;
	}

	for (uint32 i = 0; i < node->mNumChildren; i++) {
		struct aiNode *childNode = getNode(node->mChildren[i], name);
		if (childNode) {
			return childNode;
		}
	}

	return NULL;
}

void getNodesByPrefix(
	struct aiNode *node,
	struct aiNode ***nodes,
	uint32 *numNodes,
	const char *prefix,
	const char *delimiter
) {
	char *nodePrefix = getPrefix(node->mName.data, delimiter);
	if (nodePrefix && !strcmp(nodePrefix, prefix)) {
		if (*numNodes == 0) {
			*nodes = malloc(++(*numNodes) * sizeof(struct aiNode*));
		} else {
			*nodes = realloc(*nodes, ++(*numNodes) * sizeof(struct aiNode*));
		}

		(*nodes)[*numNodes - 1] = node;
	}

	free(nodePrefix);

	for (uint32 i = 0; i < node->mNumChildren; i++) {
		getNodesByPrefix(
			node->mChildren[i],
			nodes,
			numNodes,
			prefix,
			delimiter
		);
	}
}