#pragma once

#include "definitions.h"

#include "file_utilities.h"

#include <assimp/scene.h>

#include <kazmath/mat4.h>
#include <kazmath/vec3.h>
#include <kazmath/quaternion.h>

#include <cjson/cJSON.h>

#include <stdio.h>

typedef struct bone_offset_t {
	char name[1024];
	kmMat4 offset;
} BoneOffset;

int32 exportAnimations(
	const struct aiScene *scene,
	const char *parentFolder,
	cJSON *assetData,
	FILE *file,
	Log log
);

kmMat4 convertToKazmathMatrix(const struct aiMatrix4x4 *aiMatrix);
void decomposeTransform(
	const kmMat4 *transform,
	kmVec3 *position,
	kmQuaternion *rotation,
	kmVec3 *scale,
	bool removeRotationScale
);