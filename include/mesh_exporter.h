#pragma once

#include "definitions.h"

#include "file_utilities.h"

int32 exportMesh(const char *filename, Log log);