# Model Utility

This is a [CLI tool](https://en.wikipedia.org/wiki/Command-line_interface) and [shared library](https://en.wikipedia.org/wiki/Shared_library) for exporting [3D model](https://en.wikipedia.org/wiki/3D_modeling) data from [FBX](https://en.wikipedia.org/wiki/FBX) files to optimized [binary formats](https://en.wikipedia.org/wiki/Binary_file) compatible with the **[Ghoti](https://gitlab.com/psahani/ghoti)** game engine. This [utility](https://en.wikipedia.org/wiki/Utility_software) is a core part of the engine's custom [art/asset pipeline](https://en.wikipedia.org/wiki/Art_pipeline), and it adheres to a detailed [specification](https://en.wikipedia.org/wiki/Specification_(technical_standard)) (a set of [naming conventions](https://en.wikipedia.org/wiki/Naming_convention_(programming))) for preparing [meshes](https://en.wikipedia.org/wiki/Polygon_mesh), materials ([UV maps](https://en.wikipedia.org/wiki/UV_mapping)), and [collision geometry](https://en.wikipedia.org/wiki/Collision_detection#Bounding_volumes) before being imported/exported.

## Usage

The utility exports mesh and animation data to a binary format. Material, physics, and animation information is exported separately to a JSON file which can be used with the [JSON utilities](https://gitlab.com/psahani/json-utilities) for the engine. If the model has animations, the [skeleton](https://en.wikipedia.org/wiki/Skeletal_animation) is exported as entities which can be used with the engine's [ECS](https://en.wikipedia.org/wiki/Entity_component_system).

### Exporting Models

``` sh
$ ./model-utility model.fbx
```

The utility will output mesh data to a binary file named `model.mesh`. Material, physics, and animation information will be output to a JSON file named `model.json`. If there are animations in the model, the model's skeleton is exported as JSON files into a folder named `skeleton/`.

### Help

``` sh
$ ./model-utility --help
```

Example usage for the utility is shown.

## Building

### Linux

Since care has been taken to include redistributable binaries of all necessary libraries in the `lib/` folder, all that should be needed to build the CLI tool is to have [Clang](https://clang.llvm.org/) and [Make](https://www.gnu.org/software/make/) installed before running this single command:

```sh
$ make rebuild
```

The resulting binary will be placed in the `build/` folder by default.

Other makefile targets are also available including `release` (compiles a release version of the CLI tool into the `release/` folder) and `library` (builds the shared library).

### Windows

Building for Windows is only supported through [cross-compilation](https://en.wikipedia.org/wiki/Cross_compiler) from a Linux host using the [MinGW](https://www.mingw-w64.org/) compatibility environment. Redistributables are located in the `winlib/` folder. Assuming the `x86_64-w64-mingw32-clang` binary is available on the [system path](https://en.wikipedia.org/wiki/PATH_(variable)), simply run this command to build the CLI tool:

```sh
$ make windows
```

Release builds are possible by setting the appropriate [environment variable](https://en.wikipedia.org/wiki/Environment_variable):

```sh
$ WINDOWS=true make release
```

The shared library can also be cross-compiled:

```sh
$ WINDOWS=true make library
```