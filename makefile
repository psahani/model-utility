PROJ = model-utility

IDIRS = include vendor
SRCDIR = src

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj

_LIBDIRS = lib
LIBDIRS = $(foreach LIBDIR,$(_LIBDIRS),-L$(LIBDIR) -Wl,-rpath-link,$(LIBDIR))

CC = clang
CFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR)) -fPIC
DBFLAGS = -g -D_DEBUG -O0 -Wall
RELFLAGS = -O3
SHAREDFLAGS = -shared

_LIBS = file-utilities cjson frozen assimp kazmath
LIBS = $(foreach LIB,$(_LIBS),-l$(LIB))

DEPS = $(shell find include -name *.h)
VENDORDEPS = $(shell find vendor -name *.h)

OBJ = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(shell find $(SRCDIR) -name *.c -not -name main.c))

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

.PHONY: build

build : $(OBJ) $(if $(LIBRARY),,$(OBJDIR)/main.o)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(LIBDIRS) $(if $(LIBRARY),$(SHAREDFLAGS),) -o $(BUILDDIR)/$(if $(LIBRARY),lib$(PROJ).so,$(PROJ)) $^ $(LIBS)

.PHONY: library

library : clean
	@make$(if $(WINDOWS), windows,) LIBRARY=yes RELEASE=yes

.PHONY: clean

clean:
	rm -rf release
	rm -rf $(BUILDDIR)
	mkdir -p $(OBJDIR)

.PHONY: rebuild

rebuild : clean build

.PHONY: release

release : clean
	@make RELEASE=yes$(if $(WINDOWS), windows,)
	mkdir release/
	find build/* -type f -not -path '*/obj/*' -exec cp {} release/ \;
	$(if $(WINDOWS),,mv release/$(PROJ) release/$(PROJ)-bin)
	$(if $(WINDOWS),,cp -r lib/ release/)
	$(if $(WINDOWS),,echo '#!/bin/bash' > release/$(PROJ) && echo 'LD_LIBRARY_PATH=./lib ./$(PROJ)-bin $$@' >> release/$(PROJ) && chmod +x release/$(PROJ))

WINCC = x86_64-w64-mingw32-clang
WINCFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR))
WINFLAGS = -I/usr/local/include -Wl,-subsystem,windows
_WINLIBS = file-utilities cjson frozen assimp kazmath
WINLIBS = $(foreach LIB,$(_WINLIBS),-l$(LIB))

_WINLIBDIRS = winlib
WINLIBDIRS = $(foreach LIBDIR,$(_WINLIBDIRS),-L$(LIBDIR))

WINOBJ = $(patsubst %.o,%.obj,$(OBJ))

$(OBJDIR)/%.obj : $(SRCDIR)/%.c $(DEPS)
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

.PHONY: windows

windows : $(WINOBJ) $(if $(LIBRARY),,$(OBJDIR)/main.obj)
	$(WINCC) $(WINCFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) $(WINLIBDIRS) $(if $(LIBRARY),$(SHAREDFLAGS),) -o $(BUILDDIR)/$(if $(LIBRARY),$(PROJ).dll,$(PROJ).exe) $^ $(WINLIBS)
	cp winlib/* $(BUILDDIR)/
